package joda;


import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class JodaTest {

    public static void main(String[] args) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ");
        Calendar tMinus40 = new GregorianCalendar();
        tMinus40.add(Calendar.DATE, -30);

        Calendar calendar = new GregorianCalendar();
        System.out.println("now: " + dateFormat.format(calendar.getTime()));
        calendar.add(Calendar.DATE, -39);
        System.out.println(dateFormat.format(calendar.getTime()));
        System.out.println(calendar.compareTo(tMinus40));

    }
}
